package com.wp.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author 卫鹏
 * @Description 密码 加密工具类
 * @createTime 2021年12月01日 12:09:00
 */
public class BCryptPasswordEncoderUtils {

private static BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    /**
     * 输入字符串密码 返回 加密后的密码
     * @return encodePassword
     */
    public static String passwordEncode(String password){
        return bCryptPasswordEncoder.encode(password);
    }

}
