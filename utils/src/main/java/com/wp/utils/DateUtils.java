package com.wp.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 卫鹏
 * @Description Date与String的转换
 * @createTime 2021年11月16日 22:20:00
 */
public class DateUtils {
    /**
     * 日期转换成字符串
     *
     * @param date    传入的Date类型日期
     * @param pattern 要格式化的日期格式
     * @return sdf.format(date) 转换好的字符串
     */
    public static String dateToString(Date date, String pattern) {
        //创建SimpleDateFormat对象:
        DateFormat sdf = new SimpleDateFormat(pattern);
        //将转换好的字符串返回：
        return sdf.format(date);
    }

    /**
     * 字符串转换成日期
     *
     * @param str     转入的字符串类型的日期
     * @param pattern 要格式化的日期格式
     * @return sdf.parse(str) 转换好的Date
     * @throws ParseException 日期类异常
     */
    public static Date StringToDate(String str, String pattern) throws ParseException {
        //创建SimpleDateFormat对象:
        DateFormat sdf = new SimpleDateFormat(pattern);
        //将转换好的Date返回：
        return sdf.parse(str);
    }

}
