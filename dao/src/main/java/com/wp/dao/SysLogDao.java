package com.wp.dao;

import com.wp.domain.SysLog;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 日志持久层接口
 * @createTime 2021年12月09日 14:27:00
 */
public interface SysLogDao {
    /**
     * 添加日志
     * @param sysLog 日志实体类
     */
    public void save(SysLog sysLog) throws Exception;

    /**
     * 查询所有 日志表
     * @throws Exception
     */
    public List<SysLog> findAll() throws Exception;
}
