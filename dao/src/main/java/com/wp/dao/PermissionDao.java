package com.wp.dao;

import com.wp.domain.Permission;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 权限表Permission 的持久层接口
 * @createTime 2021年12月01日 20:39:00
 */
public interface PermissionDao {

    /**
     * 根据角色表role 的 id 来查询 权限表Permission的数据
     *
     * @param roleId
     * @return
     */
    public List<Permission> findPermissionByRoleId(String roleId);


    /**
     * 查询所有的权限表的信息
     *
     * @return List<Permission>
     */
    List<Permission> findAll() throws Exception;

    /**
     * 添加资源权限
     *
     * @param permission 资源权限实体类
     * @throws Exception
     */
    void save(Permission permission) throws Exception;
}
