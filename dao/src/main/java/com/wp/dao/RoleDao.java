package com.wp.dao;

import com.wp.domain.Permission;
import com.wp.domain.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 角色表 持久层接口
 * @createTime 2021年11月28日 13:46:00
 */
public interface RoleDao {
    /**
     * 根据 用户表的id来查询user_role中间表 根据中间表查询roleId，再根据roleId查询role表的所有数据
     *
     * @param userId 用户表的id
     * @return Role
     */
    List<Role> findRoleByUserId(String userId) throws Exception;

    /**
     * 查询角色表的所有信息
     *
     * @return List<Role>
     */
    List<Role> findAll() throws Exception;

    /**
     * 添加 保存用户
     *
     * @param role 用户实体类
     */
    void save(Role role) throws Exception;

    /**
     * 根据用户指定的角色表id 查询 角色的所有数据
     *
     * @param roleId 角色表的id
     */
    Role findRoleById(String roleId) throws Exception;

    /**
     * 根据角色id 查询 没有绑定此角色表的所有权限信息
     *
     * @param roleId 角色表id
     * @return List<Permission>
     */
    List<Permission> findAllOtherPermissionByRoleId(String roleId) throws Exception;

    /**
     * 将 roleId 和 permissionId 添加到 中间表role_permission 实现关联
     *
     * @param roleId       角色表id
     * @param permissionId 资源权限表id
     * @throws Exception
     */
    void addPermissionToRole(@Param("permissionId") String permissionId, @Param("roleId") String roleId) throws Exception;

    /**
     * 根据 roleId 查询 角色表的信息 和 角色id旗下的权限信息
     *
     * @param roleId 角色Id
     * @return Role
     */
    Role findRoleByIdAndPermissionByRoleId(String roleId) throws Exception;
}
