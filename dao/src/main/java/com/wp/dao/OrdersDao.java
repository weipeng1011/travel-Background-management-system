package com.wp.dao;

import com.wp.domain.Orders;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 订单表 持久层接口
 * @createTime 2021年11月19日 13:17:00
 */
public interface OrdersDao {
    /**
     * 查询 订单表的所有数据
     */
    List<Orders> findAll() throws Exception;

    /**
     * 根据指定的订单id来查询Orders的数据
     *
     * @param ordersId 订单id
     * @return Orders 封装的订单表实体类
     */
    Orders findByOrdersId(String ordersId);
}
