package com.wp.dao;

import com.wp.domain.Role;
import com.wp.domain.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 用户表 持久层接口
 * @createTime 2021年11月28日 13:14:00
 */
public interface UserDao {

    /**
     * 根据 username 来查询 Users表
     *
     * @param username 用户名
     * @return UserInfo
     */
    public UserInfo findByUsername(String username) throws Exception;


    /**
     * 查询users表的所有数据 查询users表时，顺带将role表查询出来
     *
     * @return List<UserInfo>
     */
    List<UserInfo> findAll() throws Exception;

    /**
     * 添加用户
     *
     * @param userInfo 用户实体类
     * @throws Exception
     */
    public void save(UserInfo userInfo) throws Exception;

    /**
     * 根据user的id来查询 role角色表 和 Permission权限表 的 所有数据
     *
     * @param userId 用户表的id
     * @return List<UserInfo>
     */
    UserInfo findById(String userId) throws Exception;

    /**
     * 根据用户id 查询 角色表 没有 绑定用户的所有角色信息
     *
     * @param userId 用户id
     * @return List<Role>
     */
    List<Role> findOtherRoleByUserId(String userId);

    /**
     * 将 userId 和 roleId 存储到中间表中 实现关联
     * 如果有两个参数 就要使用 @Param 去指定每个参数的名称
     *
     * @param userId 用户表的id
     * @param roleId 角色表的id
     * @throws Exception
     */
    void addRoleToUser(@Param("userId") String userId, @Param("roleId") String roleId) throws Exception;
}
