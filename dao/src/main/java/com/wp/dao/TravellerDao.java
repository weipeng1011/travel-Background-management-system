package com.wp.dao;

import com.wp.domain.Traveller;

/**
 * @author 卫鹏
 * @Description 旅客表 持久层接口
 * @createTime 2021年11月23日 20:08:00
 */
public interface TravellerDao {


    /**
     * 根据 ordersId 来查询Traveller的信息  涉及到 orders_traveller中间表 [以Traveller查询，内查询以orders_traveller中间表查询ordersId]
     * @param orderId orders表的id
     * @return Traveller实体类
     */
    Traveller findByOrderId(String orderId);
}
