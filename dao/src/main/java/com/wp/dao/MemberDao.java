package com.wp.dao;

import com.wp.domain.Member;

/**
 * @author 卫鹏
 * @Description 会员表 持久层接口
 * @createTime 2021年11月23日 17:09:00
 */
public interface MemberDao {

    /**
     * 根据id查询Member数据
     *      * @param memberId member表的id
     * @return Member
     */
        Member findByMemberId(String memberId);
}
