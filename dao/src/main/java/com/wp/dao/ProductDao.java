package com.wp.dao;

import com.wp.domain.Product;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 产品表 持久层接口
 * @createTime 2021年11月10日 16:06:00
 */
public interface ProductDao {

    /**
     * 查询产品表的所有数据
     *
     * @return List<Product>
     */
    public List<Product> findAll() throws Exception;

    /**
     * 添加产品
     *
     * @param product 产品的实体类
     * @throws Exception
     */
    public void save(Product product) throws Exception;

    /**
     * 根据指定 Orders订单表的ProductId查询产品
     *
     * @param productId 产品表id
     * @return Product
     */
    Product findByProductId(String productId);
}
