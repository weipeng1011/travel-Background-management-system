package com.wp.domain;

import java.io.Serializable;
import java.util.List;

/**
 * @author 卫鹏
 * @Description 用户表 实体类
 * @createTime 2021年11月27日 20:59:00
 */
public class UserInfo implements Serializable {
    private String id;                  //用户表的主键
    private String email;               //邮箱
    private String username;            //用户名
    private String password;            //密码(加密)
    private String phoneNum;            //电话
    private Integer status;             //状态【0：未开启 1：开启】
    private String statusStr;           //状态格式化后的字符串
    private List<Role> roles;           //角色表的List集合

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 状态【0：未开启 1：开启】
     */
    public String getStatusStr() {
        if (status != null) {
            statusStr = status == 1 ? "开启" : (status == 0 ? "未开启" : "状态异常");
        }
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phoneNum='" + phoneNum + '\'' +
                ", status=" + status +
                ", statusStr='" + statusStr + '\'' +
                ", roles=" + roles +
                '}';
    }
}
