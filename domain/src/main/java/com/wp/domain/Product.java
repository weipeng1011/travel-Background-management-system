package com.wp.domain;

import com.wp.utils.DateUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 卫鹏
 * @Description 产品表 实体类
 * @createTime 2021年11月10日 15:51:00
 */
public class Product implements Serializable {
    private String id;                  //主键
    private String productNum;          //产品编号
    private String productName;         //产品名称
    private String cityName;            //出发城市
    //格式化时间类的格式：
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date DepartureTime;         //出发时间
    private String DepartureTimeStr;    //格式化后的出发时间
    private BigDecimal productPrice;    //产品价格
    private String productDesc;         //产品描述
    private Integer productStatus;      //状态（0 关闭 1 开启）
    private String productStatusStr;    //格式化后的状态


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductNum() {
        return productNum;
    }

    public void setProductNum(String productNum) {
        this.productNum = productNum;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Date getDepartureTime() {
        return DepartureTime;
    }

    public void setDepartureTime(Date departureTime) {
        DepartureTime = departureTime;
    }

    /**
     * 将日期 转换成字符串
     */
    public String getDepartureTimeStr() {
        //判断DepartureTime是否为null:
        if (DepartureTime != null) {
            //使用工具模块的DateUtils:     传入Date日期，日期格式化的String类型
            DepartureTimeStr = DateUtils.dateToString(DepartureTime, "yyyy年MM月dd日 hh时mm分ss秒");
        }
        return DepartureTimeStr;
    }

    public void setDepartureTimeStr(String departureTimeStr) {
        DepartureTimeStr = departureTimeStr;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public Integer getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(Integer productStatus) {
        this.productStatus = productStatus;
    }

    /**
     * 状态（0 关闭 1 开启）
     */
    public String getProductStatusStr() {
        //判断productStatus是否有值：
        if (productStatus != null) {
            //0:关闭 1:开启 other:状态异常
            productStatusStr = productStatus == 1 ? "开启" : (productStatus == 0 ? "关闭" : "状态异常");
        }
        return productStatusStr;
    }

    public void setProductStatusStr(String productStatusStr) {
        this.productStatusStr = productStatusStr;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", productNum='" + productNum + '\'' +
                ", productName='" + productName + '\'' +
                ", cityName='" + cityName + '\'' +
                ", DepartureTime=" + DepartureTime +
                ", DepartureTimeStr='" + DepartureTimeStr + '\'' +
                ", productPrice=" + productPrice +
                ", productDesc='" + productDesc + '\'' +
                ", productStatus=" + productStatus +
                ", productStatusStr='" + productStatusStr + '\'' +
                '}';
    }


}