package com.wp.domain;

import com.wp.utils.DateUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 卫鹏
 * @Description 订单表实体类
 * @createTime 2021年11月18日 22:47:00
 */
public class Orders implements Serializable {
    private String id;                          //主键
    private String orderNum;                    //订单编号
    private Date orderTime;                     //下单时间
    private String orderTimeStr;                //###格式化###后的下单时间
    private Integer peopleCount;                //出行人数
    private String orderDesc;                   //订单描述(其他信息)
    private Integer payType;                    //支付方式(0:支付宝 1:微信 2:其他)
    private String payTypeStr;                  //###格式化###支付方式(0:支付宝 1:微信 2:其他)
    private Integer orderStatus;                //订单状态(0:未支付 1:已支付)
    private String orderStatusStr;              //###格式化###订单状态(0:未支付 1:已支付)
    private String productId;                   //产品编号
    private String memberId;                    //会员编号
    private Product product;                    //产品信息
    private Member member;                      //会员信息
    private List<Traveller> travellers;         //旅客信息

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    /**
     * 格式化下单时间
     *
     * @return 格式化后的下单时间
     */
    public String getOrderTimeStr() {
        //调用工具类的时间转换：
        return DateUtils.dateToString(orderTime, "yyyy年MM月dd日 HH时mm分ss秒");
    }

    public void setOrderTimeStr(String orderTimeStr) {
        this.orderTimeStr = orderTimeStr;
    }

    public Integer getPeopleCount() {
        return peopleCount;
    }

    public void setPeopleCount(Integer peopleCount) {
        this.peopleCount = peopleCount;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    /**
     * 支付方式(0:支付宝 1:微信 2:其他)
     *
     * @return 格式化后的支付方式
     */
    public Integer getPayType() {
        //判断payType是否有值:
        if (payType != null) {
            payTypeStr = payType == 0 ? "支付宝" : (payType == 1 ? "微信" : (payType == 2 ? "其他" : "显示有误"));
        }
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getPayTypeStr() {
        return payTypeStr;
    }

    public void setPayTypeStr(String payTypeStr) {
        this.payTypeStr = payTypeStr;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * 订单状态(0:未支付 1:已支付)
     *
     * @return 格式化后的订单状态
     */
    public String getOrderStatusStr() {
        //判断orderStatus是否有值:
        if (orderStatus != null) {
            orderStatusStr = orderStatus == 0 ? "未支付" : (orderStatus == 1 ? "已支付" : "显示有误");
        }
        return orderStatusStr;
    }

    public void setOrderStatusStr(String orderStatusStr) {
        this.orderStatusStr = orderStatusStr;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public List<Traveller> getTravellers() {
        return travellers;
    }

    public void setTravellers(List<Traveller> travellers) {
        this.travellers = travellers;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "id='" + id + '\'' +
                ", orderNum='" + orderNum + '\'' +
                ", orderTime=" + orderTime +
                ", orderTimeStr='" + orderTimeStr + '\'' +
                ", peopleCount=" + peopleCount +
                ", orderDesc='" + orderDesc + '\'' +
                ", payType=" + payType +
                ", payTypeStr='" + payTypeStr + '\'' +
                ", orderStatus=" + orderStatus +
                ", orderStatusStr='" + orderStatusStr + '\'' +
                ", product=" + product +
                ", member=" + member +
                ", travellers=" + travellers +
                '}';
    }
}