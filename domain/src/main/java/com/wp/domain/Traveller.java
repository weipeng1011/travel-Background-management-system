package com.wp.domain;

import java.io.Serializable;

/**
 * @author 卫鹏
 * @Description 旅客表 实体类
 * @createTime 2021年11月19日 12:26:00
 */
public class Traveller implements Serializable {
    private String id;                      //id主键
    private String name;                    //姓名
    private String sex;                     //性别
    private String phoneNum;                //电话号码
    private Integer credentialsType;        //证件类型(0:身份证 1:护照 2:军官证)
    private String credentialsTypeStr;      //###格式化###证件类型(0:身份证 1:护照 2:军官证)
    private String credentialsNum;          //证件号码
    private Integer travellerType;          //旅客类型(人群)0:成人 1:儿童
    private String travellerTypeStr;        //###格式化###旅客类型(人群)0:成人 1:儿童

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public Integer getCredentialsType() {
        return credentialsType;
    }

    public void setCredentialsType(Integer credentialsType) {
        this.credentialsType = credentialsType;
    }


    /**
     * 证件类型(0:身份证 1:护照 2:军官证)
     *
     * @return 格式化后的字符串
     */
    public String getCredentialsTypeStr() {
        //判断credentialsType是否为空：
        if (credentialsType != null) {
            credentialsTypeStr =
                    credentialsType == 0 ? "省份证" :
                            (credentialsType == 1 ? "护照" : (credentialsType == 2 ? "军官证" : "显示有误"));
        }
        return credentialsTypeStr;
    }

    public void setCredentialsTypeStr(String credentialsTypeStr) {
        this.credentialsTypeStr = credentialsTypeStr;
    }

    public String getCredentialsNum() {
        return credentialsNum;
    }

    public void setCredentialsNum(String credentialsNum) {
        this.credentialsNum = credentialsNum;
    }

    public Integer getTravellerType() {
        return travellerType;
    }

    public void setTravellerType(Integer travellerType) {
        this.travellerType = travellerType;
    }

    /**
     * 旅客类型(人群)0:成人 1:儿童
     *
     * @return 格式化后的旅客类型
     */
    public String getTravellerTypeStr() {
        //判断travellerType是否有值：
        if (travellerType != null) {
            travellerTypeStr = travellerType == 0 ? "成人" : (travellerType == 1 ? "儿童" : "显示有误");
        }
        return travellerTypeStr;
    }

    public void setTravellerTypeStr(String travellerTypeStr) {
        this.travellerTypeStr = travellerTypeStr;
    }

    @Override
    public String toString() {
        return "Traveller{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", phoneNum='" + phoneNum + '\'' +
                ", credentialsType=" + credentialsType +
                ", credentialsTypeStr='" + credentialsTypeStr + '\'' +
                ", credentialsNum='" + credentialsNum + '\'' +
                ", travellerType=" + travellerType +
                ", travellerTypeStr='" + travellerTypeStr + '\'' +
                '}';
    }
}
