package com.wp.domain;

import java.io.Serializable;
import java.util.List;

/**
 * @author 卫鹏
 * @Description 角色表的实体类
 * @createTime 2021年11月27日 21:03:00
 */
public class Role implements Serializable {
    private String id;                      //主键
    private String roleName;                //角色表
    private String roleDesc;                //角色描述
    private List<UserInfo> userInfos;       //用户表封装的list集合
    private List<Permission> permissions;   //资源权限表的List集合

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    public List<UserInfo> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id='" + id + '\'' +
                ", roleName='" + roleName + '\'' +
                ", roleDesc='" + roleDesc + '\'' +
                ", userInfos=" + userInfos +
                ", permissions=" + permissions +
                '}';
    }
}
