package com.wp.domain;

import com.wp.utils.DateUtils;

import java.util.Date;

/**
 * @author 卫鹏
 * @Description 日志表 实体类
 * @createTime 2021年12月08日 20:40:00
 */
public class SysLog {
    private String id;                  //主键
    private Date visitTime;             //访问时间
    private String visitTimeStr;          //访问时间格式化的字符串
    private String username;            //操作者用户名
    private String ip;                  //访问ip
    private String url;                 //访问资源url
    private Long executionTime;         //执行时长
    private String method;              //访问方法

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Long executionTime) {
        this.executionTime = executionTime;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getVisitTimeStr() {
        if (visitTime != null) {
            //转化为String:
            visitTimeStr = DateUtils.dateToString(visitTime, "yyyy年MM月dd日 HH时mm分ss分");
        }
        return visitTimeStr;
    }

    public void setVisitTimeStr(String visitTimeStr) {
        this.visitTimeStr = visitTimeStr;
    }

    @Override
    public String toString() {
        return "SysLog{" +
                "id='" + id + '\'' +
                ", visitTime=" + visitTime +
                ", visitTimeStr='" + visitTimeStr + '\'' +
                ", username='" + username + '\'' +
                ", ip='" + ip + '\'' +
                ", url='" + url + '\'' +
                ", executionTime=" + executionTime +
                ", method='" + method + '\'' +
                '}';
    }
}
