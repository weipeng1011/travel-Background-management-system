package com.wp.domain;

import java.io.Serializable;
import java.util.List;

/**
 * @author 卫鹏
 * @Description 资源权限表 实体类
 * @createTime 2021年11月27日 21:04:00
 */
public class Permission implements Serializable {
    private String id;                  //主键
    private String permissionName;      //权限名
    private String url;                 //资源路径
    private List<Role> roles;           //角色表封装的List集合

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "id='" + id + '\'' +
                ", permissionName='" + permissionName + '\'' +
                ", url='" + url + '\'' +
                ", roles=" + roles +
                '}';
    }
}
