package com.wp.controller;

import com.github.pagehelper.PageInfo;
import com.wp.domain.Permission;
import com.wp.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 权限表 视图层接口
 * @createTime 2021年12月03日 16:11:00
 */
@Controller
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @RequestMapping("/findAll")
    public ModelAndView findAll(
            @RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
            @RequestParam(name = "size", required = true, defaultValue = "4") Integer size
    ) throws Exception {
        //创建ModelAndVIew对象:
        ModelAndView mav = new ModelAndView();
        //调用permissionService的findAll方法:
        List<Permission> permissions = permissionService.findAll(page, size);
        //调用分页类:
        PageInfo<Permission> pageInfo = new PageInfo<>(permissions);
        //将数据存储到request域中:
        mav.addObject("pageInfo", pageInfo);
        //跳转页面:
        mav.setViewName("permission-list");

        //返回对象:
        return mav;
    }


    /**
     * 添加 资源权限信息
     *
     * @param permission 资源权限实体类
     * @return "redirect:/role/findAll"
     * @throws Exception
     */
    @RequestMapping("/save")
    public String save(Permission permission) throws Exception {
        //调用业务层的添加资源权限的业务逻辑操作：
        permissionService.save(permission);
        //使用重定向来加载查询所有资源:
        return "redirect:/permission/findAll";
    }
}
