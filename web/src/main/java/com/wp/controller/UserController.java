package com.wp.controller;

import com.github.pagehelper.PageInfo;
import com.wp.domain.Role;
import com.wp.domain.UserInfo;
import com.wp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 用户 视图层接口
 * @createTime 2021年11月30日 16:35:00
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 查询所有User用户并分页
     *
     * @param page 当前页
     * @param size 每页展示的数据条数
     * @return ModelAndView
     */
    @RequestMapping("/findAll")
    public ModelAndView findAll(
            @RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
            @RequestParam(name = "size", required = true, defaultValue = "4") Integer size) throws Exception {
        //创建ModelAndView对象:
        ModelAndView mav = new ModelAndView();
        //调用服务层的findAll方法：
        List<UserInfo> userInfos = userService.findAll(page, size);
        //调用PageInfo对象:
        PageInfo<UserInfo> pageInfo = new PageInfo<>(userInfos);
        //将pageInfo对象存储到request域中:
        mav.addObject("pageInfo", pageInfo);
        //设置跳转的页面:
        mav.setViewName("user-list");
        //将ModelAndView对象返回：
        return mav;
    }

    /**
     * 添加用户
     *
     * @param userInfo 用户实体类
     * @return 重定向到查询所有用户
     */
    @RequestMapping("/save")
    public String save(UserInfo userInfo) throws Exception {

        //调用userService业务层的保存用户的方法：
        userService.save(userInfo);

        //重定向到查询所有用户上：
        return "redirect:/user/findAll";
    }

    /**
     * 根据用户表user的id来查询 角色表role 和 权限表Permission 的信息
     *
     * @param userId user表的id
     * @return ModelAndView
     */
    @RequestMapping("/findById/{id}")
    public ModelAndView findById(@PathVariable("id") String userId) throws Exception {
        //创建ModelAndView对象:
        ModelAndView mav = new ModelAndView();
        //调用userService业务层接口的findById 返回 List<User>:
        UserInfo user = userService.findById(userId);
        //将 user实体类 存储到request域中:
        mav.addObject("user", user);
        //设置跳转页面：
        mav.setViewName("user-show");
        //将ModelAndView返回：
        return mav;
    }

    /**
     * 根据 UserId进行查询 跳转页面 / 查询此UserId没有的roleId包含的角色数据
     * UserInfo :用户实体类 存放用户的信息
     *
     * @param userId 用户表的id
     * @return ModelAndView
     */
    @RequestMapping("/findUserByIdAndAllOtherRole/{id}")
    public ModelAndView findUserByIdAndAllOtherRole(@PathVariable("id") String userId) throws Exception {
        //创建ModelAndView对象:
        ModelAndView mav = new ModelAndView();

        //调用UserService的findById进行查询User的数据：
        UserInfo userInfo = userService.findById(userId);
        //将用户实体类存储到request域中:
        mav.addObject("user", userInfo);

        //调用UserService的findOtherRoleByUserId进行查询Role的数据：
        List<Role> otherRoles = userService.findOtherRoleByUserId(userId);
        //将角色实体类的集合存储到request域中:
        mav.addObject("roleList", otherRoles);

        //设置跳转的页面:
        mav.setViewName("user-role-add");

        //将ModelAndView对象返回:
        return mav;
    }

    /**
     * 获取 用户表id 和 角色表id 添加到中间表user_role 来完成关联操作
     */
    @RequestMapping("/addRoleToUser")
    //只有用户名为weipeng才能访问此方法
    @PreAuthorize("authentication.principal.username=='weipeng'")
    public String addRoleToUser(@RequestParam(name = "userId", required = true) String userId,
                                @RequestParam(name = "ids", required = true) String[] roleIds) throws Exception {

        //调用userService将角色id 和 用户的id 存储到 中间表 user_role中:
        userService.addRoleToUser(userId, roleIds);

        //使用重定向调用查询所以用户：
        return "redirect:/user/findAll";
    }



}
