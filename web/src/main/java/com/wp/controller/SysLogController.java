package com.wp.controller;

import com.github.pagehelper.PageInfo;
import com.wp.domain.SysLog;
import com.wp.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 日志表 视图层接口
 * @createTime 2021年12月09日 20:49:00
 */
@RequestMapping("/sysLog")
@Controller
public class SysLogController {

    @Autowired
    private SysLogService sysLogService;

    @RequestMapping("/findAll")
    public ModelAndView findAll(
            @RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
            @RequestParam(name = "size", required = true, defaultValue = "4") Integer size
    ) throws Exception {
        //创建ModelAndView对象：
        ModelAndView mav = new ModelAndView();
        //调用业务层接口:
        List<SysLog> sysLogList = sysLogService.findAll(page, size);
        //调用PageInfo分页类:
        PageInfo<SysLog> pageInfo = new PageInfo<>(sysLogList);
        //存储到request域中:
        mav.addObject("sysLogs", pageInfo);
        //设置跳转页面:
        mav.setViewName("syslog-list");

        //将mav对象返回:
        return mav;
    }

}
