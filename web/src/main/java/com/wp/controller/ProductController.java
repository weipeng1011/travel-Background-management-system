package com.wp.controller;

import com.github.pagehelper.PageInfo;
import com.wp.domain.Product;
import com.wp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.RolesAllowed;
import java.util.List;

/**
 * @author 卫鹏
 * @Description 产品表 视图层接口
 * @createTime 2021年11月10日 17:49:00
 */
@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping("findAll")
    public ModelAndView findAll(
            @RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
            @RequestParam(name = "size", required = true, defaultValue = "4") Integer size
    ) throws Exception {
        //创建ModelAndView对象:
        ModelAndView mav = new ModelAndView();
        //获取Product集合：
        List<Product> products = productService.findAll(page, size);
        //创建PageInfo对象:
        PageInfo<Product> pageInfo = new PageInfo<>(products);
        //存储到request域中:
        mav.addObject("pageInfo", pageInfo);
        //转发到页面：
        mav.setViewName("all_product_list");
        //将ModelAndView对象返回：
        return mav;
    }

    @RequestMapping("/save")
    public String Save(Product product) throws Exception {
        //调用业务层的添加产品的业务逻辑操作：
        productService.save(product);
        //使用重定向来加载查询所有产品：
        return "redirect:/product/findAll";
    }
}
