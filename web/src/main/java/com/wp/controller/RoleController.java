package com.wp.controller;

import com.github.pagehelper.PageInfo;
import com.wp.domain.Permission;
import com.wp.domain.Role;
import com.wp.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 角色表 视图层接口
 * @createTime 2021年12月03日 13:08:00
 */
@Controller
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;


    @RequestMapping("/findAll")
    public ModelAndView findAll(
            @RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
            @RequestParam(name = "size", required = true, defaultValue = "4") Integer size
    ) throws Exception {
        //创建ModelAndView对象:
        ModelAndView mav = new ModelAndView();
        //调用roleService的findAll：
        List<Role> roles = roleService.findAll(page, size);
        //使用PageInfo进行分页：
        PageInfo<Role> pageInfo = new PageInfo<>(roles);
        //将分页数据存储到request域中:
        mav.addObject("roleList", pageInfo);
        //转发页面：
        mav.setViewName("role-list");

        //返回ModelAndView:
        return mav;
    }

    /**
     * 添加 角色信息
     *
     * @param role 角色实体类
     * @return "redirect:/role/findAll"
     * @throws Exception
     */
    @RequestMapping("/save")
    public String save(Role role) throws Exception {
        //调用业务层的添加角色的业务逻辑操作：
        roleService.save(role);
        //使用重定向来加载查询所有角色：
        return "redirect:/role/findAll";
    }

    /**
     * 根据 RoleId进行查询 跳转页面 / 查询此RoleId没有绑定此角色表的所有权限信息
     *
     * @param roleId 角色表id
     * @return ModelAndView
     * @throws Exception
     */
    @RequestMapping("/findRoleByIdAndAllOtherPermission/{id}")
    public ModelAndView findRoleByIdAndAllOtherPermission(@PathVariable("id") String roleId) throws Exception {
        //创建ModelAndView对象:
        ModelAndView mav = new ModelAndView();

        //调用roleService 传入roleId 返回 整个role角色表的信息：
        Role role = roleService.findRoleById(roleId);
        //将role存储到request域中:
        mav.addObject("role", role);

        //调用roleService 传入roleId 返回 没有绑定此角色表的所有权限信息：
        List<Permission> permissionList = roleService.findAllOtherPermissionByRoleId(roleId);
        //将资源权限集合permissionList存储到request域中：
        mav.addObject("permissionList", permissionList);

        //跳转页面:
        mav.setViewName("role-permission-add");

        //将ModelAndView对象返回:
        return mav;
    }


    /**
     * 获取 角色表id 和 资源权限表id 添加到中间表role_permission 来完成关联操作
     */
    @RequestMapping("/addPermissionToRole")
    public String addPermissionToRole(
            @RequestParam(name = "roleId", required = true) String roleId,
            @RequestParam(name = "ids", required = true) String[] permissionIds
    ) throws Exception {

        //调用roleService 将 roleId 和 permissionIds 添加到 role_permission中间表 实现关联：
        roleService.addPermissionToRole(roleId, permissionIds);

        //重定向到查询所有角色:
        return "redirect:/role/findAll";
    }

    @RequestMapping("/findById/{id}")
    public ModelAndView findById(@PathVariable("id") String roleId) throws Exception {
        //创建ModelAndView对象:
        ModelAndView mav = new ModelAndView();
        //调用service 查询 角色表的信息和角色表管理的权限信息:
        Role role = roleService.findRoleByIdAndPermissionByRoleId(roleId);
        //将数据存储到request域中:
        mav.addObject("roleList", role);
        //跳转页面：
        mav.setViewName("role-show");
        //将mav对象返回:
        return mav;
    }


}
