package com.wp.controller;

import com.github.pagehelper.PageInfo;
import com.wp.domain.Orders;
import com.wp.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 订单表 视图层接口
 * @createTime 2021年11月19日 16:03:00
 */
@Controller
@RequestMapping("/orders")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @RequestMapping("/findAll")
    public ModelAndView findAll(@RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
                                @RequestParam(name = "size", required = true, defaultValue = "4") Integer size) throws Exception {
        //创建ModelAndView对象:
        ModelAndView mav = new ModelAndView();
        //查询所有订单表的所有数据:
        List<Orders> ordersList = ordersService.findAll(page, size);
        System.out.println(ordersList);
        //创建PageInfo分页对象：
        PageInfo<Orders> pageInfo = new PageInfo<>(ordersList);
        //将分页对象存储到request域中:
        mav.addObject("pageInfo", pageInfo);
        //设置跳转页面：
        mav.setViewName("orders-list");
        //将ModelAndView对象返回:
        return mav;
    }

    @RequestMapping("findById/{id}")
    public ModelAndView findById(@PathVariable("id") String ordersId) {
        //创建ModelAndView对象:
        ModelAndView mav = new ModelAndView();
        //调用findById:
        Orders orders = ordersService.findById(ordersId);
        mav.addObject("orders", orders);
        mav.setViewName("orders-show");
        //将ModelAndView对象返回:
        return mav;
    }

}
