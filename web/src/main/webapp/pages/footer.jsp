<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<footer class="main-footer">

    <div style="display: flex;align-items: center;justify-content: center;">
        <div style="width: 300px;height: 15px;font-size: 13px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
            <strong>Copyright &copy; 2021 <a href="https://gitee.com/weipeng1011">技术团队：星星制作者</a></strong>
        </div>

        <div style="width: 200px;height: 15px;font-size: 13px;">
            <a href="https://beian.miit.gov.cn/" target="_blank"
               style="margin-left: 10px;text-decoration: none;color: #747474;">皖ICP备2021016850号</a>
        </div>

    </div>

</footer>
