package com.wp.service;

import com.wp.domain.Orders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 订单表  业务层接口
 * @createTime 2021年11月19日 16:15:00
 */
public interface OrdersService {

    /**
     * 查询 订单表的所有数据
     */
    List<Orders> findAll(Integer page,Integer size) throws Exception;

    /**
     * 根据指定的订单id来查询Orders的数据
     *
     * @param ordersId 订单id
     * @return Orders 封装的订单表实体类
     */
    Orders findById(String ordersId);
}
