package com.wp.service;

import com.wp.domain.Role;
import com.wp.domain.UserInfo;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 用户表 业务层接口
 * @createTime 2021年11月28日 13:05:00
 */
public interface UserService extends UserDetailsService {
    /**
     * 查询users表的所有数据
     *
     * @param page 当前的页码
     * @param size 每页展示的数据
     * @return List<UserInfo>
     */
    public List<UserInfo> findAll(Integer page, Integer size) throws Exception;

    /**
     * 添加用户
     *
     * @param userInfo 用户实体类
     * @throws Exception
     */
    public void save(UserInfo userInfo) throws Exception;


    /**
     * 根据user的id来查询 role角色表 和 Permission权限表 的 所有数据
     *
     * @param userId 用户表的id
     * @return List<UserInfo>
     */
    UserInfo findById(String userId) throws Exception;

    /**
     * 根据用户id 查询 角色表 没有 绑定用户的所有角色信息
     *
     * @param userId 用户id
     * @return List<Role>
     */
    List<Role> findOtherRoleByUserId(String userId);

    /**
     * 将String[] roleIds 遍历 再将roleId 和 userId 存储到 中间表中
     *
     * @param userId  用户表的id
     * @param roleIds 角色表的id
     */
    void addRoleToUser(String userId, String[] roleIds) throws Exception;
}

