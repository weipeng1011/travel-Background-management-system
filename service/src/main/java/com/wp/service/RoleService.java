package com.wp.service;

import com.wp.domain.Permission;
import com.wp.domain.Role;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 角色表 业务层接口
 * @createTime 2021年12月03日 13:12:00
 */
public interface RoleService {
    /**
     * 查询角色表的所有信息
     *
     * @param page 当前页
     * @param size 页的数量
     * @return List<Role>
     */
    List<Role> findAll(Integer page, Integer size) throws Exception;

    /**
     * 添加 保存用户
     *
     * @param role 用户实体类
     */
    void save(Role role) throws Exception;

    /**
     * 根据用户指定的角色表id 查询 角色的所有数据
     *
     * @param roleId 角色表的id
     */
    Role findRoleById(String roleId) throws Exception;

    /**
     * 根据角色id 查询 没有绑定此角色表的所有权限信息
     *
     * @param roleId 角色表id
     * @return List<Permission>
     */
    List<Permission> findAllOtherPermissionByRoleId(String roleId) throws Exception;

    /**
     * 遍历资源权限表的id 将 roleId 和 遍历过后的permissionId 添加到 中间表role_permission 实现关联
     *
     * @param roleId        角色表id
     * @param permissionIds 资源权限表id
     * @throws Exception
     */
    void addPermissionToRole(String roleId, String[] permissionIds) throws Exception;

    /**
     * 根据 roleId 查询 角色表的信息 和 角色id旗下的权限信息
     *
     * @param roleId 角色Id
     * @return List<Role>
     */
    Role findRoleByIdAndPermissionByRoleId(String roleId) throws Exception;
}

