package com.wp.service;

import com.wp.domain.Permission;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 权限表 业务层接口
 * @createTime 2021年12月03日 16:12:00
 */
public interface PermissionService {

    /**
     * 查询所有的权限表的信息
     *
     * @param page 当前页
     * @param size 页中展示的条数
     * @return List<Permission>
     */
    List<Permission> findAll(Integer page, Integer size) throws Exception;

    /**
     * 添加保存用户
     *
     * @param permission 资源权限实体类
     */
    void save(Permission permission) throws Exception;
}
