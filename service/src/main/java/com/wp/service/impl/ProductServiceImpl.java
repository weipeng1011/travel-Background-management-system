package com.wp.service.impl;

import com.github.pagehelper.PageHelper;
import com.wp.dao.ProductDao;
import com.wp.domain.Product;
import com.wp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @author 卫鹏
 * @Description 产品表 业务层接口实现类
 * @createTime 2021年11月10日 16:27:00
 */
@Service
//开启事务：
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public List<Product> findAll(Integer page, Integer size) throws Exception {
        //调用PageHelper进行分页：
        PageHelper.startPage(page, size);
        return productDao.findAll();
    }

    @Override
    public void save(Product product) throws Exception {
        //生成id的随机数：
        String id = UUID.randomUUID().toString().replaceAll("-", "");
        //将生成的id存储到product：
        product.setId(id);
        //调用持久层的添加产品方法：
        productDao.save(product);
    }
}
