package com.wp.service.impl;

import com.github.pagehelper.PageHelper;
import com.wp.dao.UserDao;
import com.wp.domain.Role;
import com.wp.domain.UserInfo;
import com.wp.service.UserService;
import com.wp.utils.BCryptPasswordEncoderUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author 卫鹏
 * @Description 用户表 业务层接口 实现类
 * @createTime 2021年11月28日 13:06:00
 */
@Service("userService")
//开启事务：
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //创建UserInfo对象：
        UserInfo userInfo = null;
        try {
            userInfo = userDao.findByUsername(username);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //处理自己的用户对象封装成UserDetails【创建Spring-Security提供的User对象】:
        //User user = new User(userInfo.getUsername(), "{noop}" + userInfo.getPassword(), getAuthorities(userInfo.getRoles()));
        User user = new User(userInfo.getUsername(), userInfo.getPassword(), userInfo.getStatus() == 0 ? false : true, true, true, true, getAuthorities(userInfo.getRoles()));
        //将实现UserDetails的User对象返回：                                      账户是否可用：使用状态码判断
        return user;
    }

    /**
     * 返回 List集合 集合装入的是权限角色的描述
     *
     * @return List<SimpleGrantedAuthority>  SimpleGrantedAuthority是GrantedAuthority子类
     */
    public List<SimpleGrantedAuthority> getAuthorities(List<Role> roles) {
        List<SimpleGrantedAuthority> lists = new ArrayList<>();
        //遍历role角色表的集合:
        for (Role role : roles) {
            lists.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleName()));
        }
        return lists;
    }


    /**
     * 查询users表的所有数据
     *
     * @param page 当前的页码
     * @param size 每页展示的数据
     * @return List<UserInfo>
     */
    @Override
    public List<UserInfo> findAll(Integer page, Integer size) throws Exception {
        //调用PageHelper进行分页：
        PageHelper.startPage(page, size);
        return userDao.findAll();
    }

    /**
     * 添加用户
     *
     * @param userInfo 用户实体类
     * @throws Exception
     */
    @Override
    public void save(UserInfo userInfo) throws Exception {
        //设置随机生成id:
        String userId = UUID.randomUUID().toString().replaceAll("-", "");
        //将userId放入到userInfo对象中:
        userInfo.setId(userId);
        //获取userInfo中的密码并加密：
        String encodePassword = BCryptPasswordEncoderUtils.passwordEncode(userInfo.getPassword());
        //将加密后的密码再存入到userInfo实体类中：
        userInfo.setPassword(encodePassword);
        //调用userDao的持久层接口的save方法 添加用户：
        userDao.save(userInfo);
    }

    /**
     * 根据user的id来查询 role角色表 和 Permission权限表 的 所有数据
     *
     * @param userId 用户表的id
     * @return List<UserInfo>
     */
    @Override
    public UserInfo findById(String userId) throws Exception {
        return userDao.findById(userId);
    }

    /**
     * 根据用户id 查询 角色表 没有 绑定用户的所有角色信息
     *
     * @param userId 用户id
     * @return List<Role>
     */
    @Override
    public List<Role> findOtherRoleByUserId(String userId) {
        return userDao.findOtherRoleByUserId(userId);
    }

    /**
     * 将String[] roleIds 遍历 再将roleId 和 userId 存储到 中间表中
     *
     * @param userId  用户表的id
     * @param roleIds 角色表的id
     */
    @Override
    public void addRoleToUser(String userId, String[] roleIds) throws Exception {
        //遍历角色表的id:
        for (String roleId : roleIds) {
            //调用userDao存储roleId和userId到中间表 实现关联：
            userDao.addRoleToUser(userId,roleId);
        }
    }
}
