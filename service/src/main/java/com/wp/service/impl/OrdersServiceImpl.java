package com.wp.service.impl;

import com.github.pagehelper.PageHelper;
import com.wp.dao.OrdersDao;
import com.wp.domain.Orders;
import com.wp.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 订单表  业务层接口
 * @createTime 2021年11月19日 16:17:00
 */
@Service
//开启事务
@Transactional
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrdersDao ordersDao;

    @Override
    public List<Orders> findAll(Integer page, Integer size) throws Exception {
        //设置 多少页，每页展示的条数：
        PageHelper.startPage(page, size);
        return ordersDao.findAll();
    }

    @Override
    public Orders findById(String ordersId) {
        return ordersDao.findByOrdersId(ordersId);
    }
}
