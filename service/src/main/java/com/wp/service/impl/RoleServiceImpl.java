package com.wp.service.impl;

import com.github.pagehelper.PageHelper;
import com.wp.dao.RoleDao;
import com.wp.domain.Permission;
import com.wp.domain.Role;
import com.wp.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @author 卫鹏
 * @Description 角色表 业务层实现类
 * @createTime 2021年12月03日 13:14:00
 */
@Service
//开启事务
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    /**
     * 查询角色表的所有信息
     *
     * @param page 当前页
     * @param size 页的数量
     * @return List<Role>
     */
    @Override
    public List<Role> findAll(Integer page, Integer size) throws Exception {
        //进行分页:
        PageHelper.startPage(page, size);
        return roleDao.findAll();
    }

    /**
     * 添加 保存用户
     *
     * @param role 用户实体类
     */
    @Override
    public void save(Role role) throws Exception {
        //生成 随机id：
        String roleId = UUID.randomUUID().toString().replaceAll("-", "");
        //将随机id存入role实体类中:
        role.setId(roleId);
        //调用dao执行添加操作：
        roleDao.save(role);
    }

    /**
     * 根据用户指定的角色表id 查询 角色的所有数据
     *
     * @param roleId 角色表的id
     */
    @Override
    public Role findRoleById(String roleId) throws Exception {
        return roleDao.findRoleById(roleId);
    }


    /**
     * 根据角色id 查询 没有绑定此角色表的所有权限信息
     *
     * @param roleId 角色表id
     * @return List<Permission>
     */
    @Override
    public List<Permission> findAllOtherPermissionByRoleId(String roleId) throws Exception {
        return roleDao.findAllOtherPermissionByRoleId(roleId);
    }

    /**
     * 遍历资源权限表的id 将 roleId 和 遍历过后的permissionId 添加到 中间表role_permission 实现关联
     *
     * @param roleId        角色表id
     * @param permissionIds 字符数组 资源权限表id
     * @throws Exception
     */
    @Override
    public void addPermissionToRole(String roleId, String[] permissionIds) throws Exception {
        //遍历 String[] permissionIds：
        for (String permissionId : permissionIds) {

            //调用RoleDao 将roleId 和 permissionId 添加到 中间表role_permission 实现关联：
            roleDao.addPermissionToRole(permissionId,roleId);
        }
    }

    /**
     * 根据 roleId 查询 角色表的信息 和 角色id旗下的权限信息
     *
     * @param roleId 角色Id
     * @return List<Role>
     */
    @Override
    public Role findRoleByIdAndPermissionByRoleId(String roleId) throws Exception {
        return roleDao.findRoleByIdAndPermissionByRoleId(roleId);
    }
}
