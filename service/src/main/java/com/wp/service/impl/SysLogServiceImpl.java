package com.wp.service.impl;

import com.github.pagehelper.PageHelper;
import com.wp.dao.SysLogDao;
import com.wp.domain.SysLog;
import com.wp.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @author 卫鹏
 * @Description 日志业务层实现类
 * @createTime 2021年12月09日 14:20:00
 */
@Service
//开启事务：
@Transactional
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogDao sysLogDao;

    /**
     * 添加日志
     *
     * @param sysLog 日志实体类
     */
    @Override
    public void save(SysLog sysLog) throws Exception {
        //生成随机id:
        String sysLogId = UUID.randomUUID().toString().replaceAll("-", "");
        //将id存储到sysLog:
        sysLog.setId(sysLogId);
        //调用持久层接口存储数据:
        sysLogDao.save(sysLog);
    }

    /**
     * @param page 当前的页码
     * @param size 每页展示的数据
     * @return List<SysLog> 日志实体类集合
     * @throws Exception
     */
    @Override
    public List<SysLog> findAll(Integer page, Integer size) throws Exception {
        //调用分页:
        PageHelper.startPage(page, size);
        //调用持久层接口查询所有:
        return sysLogDao.findAll();
    }
}
