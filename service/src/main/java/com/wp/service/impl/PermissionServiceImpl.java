package com.wp.service.impl;

import com.github.pagehelper.PageHelper;
import com.wp.dao.PermissionDao;
import com.wp.domain.Permission;
import com.wp.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @author 卫鹏
 * @Description 权限表 业务层实现类
 * @createTime 2021年12月03日 16:16:00
 */
@Service
//开启事务
@Transactional
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionDao permissionDao;

    /**
     * 查询所有的权限表的信息
     *
     * @param page 当前页
     * @param size 页中展示的条数
     * @return List<Permission>
     */
    @Override
    public List<Permission> findAll(Integer page, Integer size) throws Exception {
        //开启分页：
        PageHelper.startPage(page, size);
        return permissionDao.findAll();
    }

    /**
     * 添加资源权限
     *
     * @param permission 资源权限实体类
     */
    @Override
    public void save(Permission permission) throws Exception {
        //生成随机id：
        String permissionId = UUID.randomUUID().toString().replaceAll("-", "");
        //将随机生成的id 存储到Permission实体类中:
        permission.setId(permissionId);
        //调用持久层接口 添加资源：
        permissionDao.save(permission);
    }
}
