package com.wp.service;

import com.wp.domain.Product;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 产品表 业务层接口
 * @createTime 2021年11月10日 16:18:00
 */
public interface ProductService {

    /**
     * 查询产品表的所有数据
     *
     * @return List<Product>
     */
    public List<Product> findAll(Integer page,Integer size) throws Exception;

    /**
     * 添加产品
     * <p>
     * product表的id在service中用UUID生成
     *
     * @param product 产品的实体类
     * @throws Exception
     */
    public void save(Product product) throws Exception;
}
