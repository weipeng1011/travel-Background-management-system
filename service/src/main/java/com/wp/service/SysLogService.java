package com.wp.service;

import com.wp.domain.SysLog;

import java.util.List;

/**
 * @author 卫鹏
 * @Description 日志业务层接口
 * @createTime 2021年12月09日 14:20:00
 */
public interface SysLogService {

    /**
     * 添加日志
     *
     * @param sysLog 日志实体类
     */
    public void save(SysLog sysLog) throws Exception;

    /**
     * @param page 当前的页码
     * @param size 每页展示的数据
     * @return List<SysLog> 日志实体类集合
     * @throws Exception
     */
    public List<SysLog> findAll(Integer page, Integer size) throws Exception;

}
